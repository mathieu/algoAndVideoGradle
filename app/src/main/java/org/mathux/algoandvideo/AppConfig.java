package org.mathux.algoandvideo;

public class AppConfig {
	public static final boolean DEBUG = false; // Enable printing debug
	public static final boolean TRACE_PERFORMANCE = false; // Enable Android trace system
	public static final int DEFAULT_DETECTION = CameraPreview.EDGE_DETECTION; // Detection used by default
	public static final boolean SCALE_IMAGE = false; //Scale image to Screen Size
}
