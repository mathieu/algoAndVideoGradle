package org.mathux.algoandvideo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Debug;
import android.os.Environment;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

public class CameraPreview extends Activity {

	public static final String KEY_TRAITEMENT = "traitement";

	public static final int PICTURE_HEIGHT = 320;
	public static final int PICTURE_WIDTH = 380;
	private static final String TAG = "CameraApiTest";


	public static final int MULTIPLEFACE_DETECTION = 0;
	public static final int SINGLEFACE_DETECTION = 1;
	public static final int EDGE_DETECTION = 2;
	public static final int EDGE_DETECTION_FILTER = 3;
	public static final int EDGE_DETECTION_RED = 4;
	public static final int WARHOLD = 5;
	public static final int RGB2HSV = 6;
	public static final int GREY = 7;
	public static final int COLOR = 8;
	public static final int NONE = 100;

	

	private int mCurrectDetection = AppConfig.DEFAULT_DETECTION;

	Camera mCamera;
	boolean mPreviewRunning = false;
	public OpenCV opencv = new OpenCV();
	


	private SurfaceView mSurfaceView;
	private RenderView mRenderView;
	
	private SparseArray<String> detectionId2detectionDesc;

	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		detectionId2detectionDesc = new SparseArray<String>();
		
		detectionId2detectionDesc.put(MULTIPLEFACE_DETECTION	, "Multi Face detection"		);
		detectionId2detectionDesc.put(SINGLEFACE_DETECTION		, "Single Face detection"		);
		detectionId2detectionDesc.put(EDGE_DETECTION			, "Edge detection"				);
		detectionId2detectionDesc.put(EDGE_DETECTION_RED		, "Edge detection in red"		);
		detectionId2detectionDesc.put(EDGE_DETECTION_FILTER		, "Edge detection best quality"	);
		detectionId2detectionDesc.put(WARHOLD					, "Warhold effect"				);
		detectionId2detectionDesc.put(GREY						, "Grey Levels"					);
		detectionId2detectionDesc.put(RGB2HSV					, "Transformation to HSV"		);
		detectionId2detectionDesc.put(NONE						, "None"						);
		

		Log.e(TAG, "onCreate");

		getWindow().setFormat(PixelFormat.TRANSLUCENT);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window win = getWindow();
		win.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setLayout(mCurrectDetection);
		

		
		
		if(AppConfig.TRACE_PERFORMANCE) {
			Debug.startMethodTracing("calc");
		}

		/* Create haarfile if necessary */
		
		String state = Environment.getExternalStorageState();
		if(!Environment.MEDIA_MOUNTED.equals(state)) {
        	Toast.makeText(this, R.string.cannot_access_sdcard
                    , Toast.LENGTH_LONG).show();

		}else {
			String fileName = "haarcascade_frontalface_alt.xml";
			File sdCardFile = Environment.getExternalStorageDirectory();
			File haarFile =  new File(sdCardFile, fileName);
			if(!haarFile.exists()) {
				if( Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
		        	Toast.makeText(this, R.string.cannot_write_sdcard
		                    , Toast.LENGTH_LONG).show();
				}else {
					try {
						haarFile.createNewFile();
				        InputStream is = getResources().openRawResource(R.raw.haarcascade_frontalface_alt);
				        OutputStream os = new FileOutputStream(haarFile);
				        byte[] data = new byte[is.available()];
				        is.read(data);
				        os.write(data);
				        is.close();
				        os.close();
					} catch (IOException e) {
						Log.d(TAG, e.toString());
						
					}


				}
			}
			
			
			
		}
	}




	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(0, COLOR					, 0, "Color");
		menu.add(0, GREY					, 0, "Grey level");
		menu.add(0, MULTIPLEFACE_DETECTION	, 0, "Face detection");
		menu.add(0, EDGE_DETECTION			, 0, "Edge detection");
		menu.add(0, EDGE_DETECTION_FILTER	, 0, "Edge Filter");
		/*menu.add(0, EDGE_DETECTION_RED, 0, "2nd Edge detection");
		menu.add(0, EDGE_DETECTION_FILTER, 0, "Edge Filter");
		menu.add(0, WARHOLD, 0, "Warhol effect");
		menu.add(0, STREAK, 0, "Streak effect");
		menu.add(0, RGB2HSV, 0, "ToHSV");
		menu.add(0, NONE, 0, "None");*/
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		Log.d(TAG, "Menu Item Selected " + item.getItemId());
		mCurrectDetection = item.getItemId();
		setLayout(mCurrectDetection);
		return true;
	}
	
	private void setLayout(int layoutID){
		switch (layoutID) {
		case COLOR:
			setContentView(R.layout.colorview);
			break;
		case GREY:
			setContentView(R.layout.main);
			break;
		case MULTIPLEFACE_DETECTION:
			setContentView(R.layout.multifaceview);
			break;
		case EDGE_DETECTION:
			setContentView(R.layout.edgeview);
			break;
		case EDGE_DETECTION_FILTER:
			setContentView(R.layout.edgefilterview);
			break;

		default:
			break;
		}
		mSurfaceView = (SurfaceView) findViewById(R.id.camera_preview);
		mRenderView = (RenderView) findViewById(R.id.image_processing);
		mRenderView.setKeepScreenOn(true);
		mRenderView.setSurfaceView(mSurfaceView);
		mRenderView.setOnLongClickListener(new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View arg0) {
				Log.e(TAG, "onLongClick");
				//mRenderView.startPicture();
				return true;
			}
		});
	}


	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if(AppConfig.TRACE_PERFORMANCE) {
				Debug.stopMethodTracing();
			}
			return super.onKeyDown(keyCode, event);
		}

		if (keyCode == KeyEvent.KEYCODE_SPACE
				|| keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
			if(AppConfig.TRACE_PERFORMANCE) {
				Debug.stopMethodTracing();
			}else {
				//mCamera.autoFocus(mFocusCallback);
			}

			return true;
		}

		return false;
	}

	protected void onResume() {
		Log.e(TAG, "onResume");
		super.onResume();
	}


	protected void onStop() {
		Log.e(TAG, "onStop");

		super.onStop();
	}
	

}
