package org.mathux.algoandvideo;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.util.Log;

public class ColorRenderer extends RenderView {

	private static final String TAG = "ColorRender";
	Bitmap	mBmp;
	NativeTools mTools = new NativeTools();
	NativeProcessing mIp = new NativeProcessing();
	int [] pixels;
	
	public ColorRenderer(Context context) {
		super(context);
	}
	
	public ColorRenderer(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	
	@Override
	protected Bitmap processFrame(byte[] data) {
		mIp.nv12ToBitmap(data, mFrameWidth, mFrameHeight, mBmp);
		return mBmp;
	}

	@Override
	protected void setPreviewSize(int width, int height) {
		Log.i(TAG, "Preview Size is "+width + " x "+height);
		if (mBmp != null){
			mBmp.recycle();
		}
		mBmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
	}

}
