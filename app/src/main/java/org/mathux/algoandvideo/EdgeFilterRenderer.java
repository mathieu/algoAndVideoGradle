package org.mathux.algoandvideo;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.util.Log;

public class EdgeFilterRenderer extends RenderView {

	private static final String TAG = "EdgeFilterRender";

	Bitmap	mBmp;
	NativeTools mTools = new NativeTools();
	NativeProcessing mIp = new NativeProcessing();
	int [] pixels;


	
	public EdgeFilterRenderer(Context context) {
		super(context);
	}

	public EdgeFilterRenderer(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected Bitmap processFrame(byte[] data) {
		mIp.nv12ToRgba(data, mFrameWidth, mFrameHeight, pixels);
		mIp.edgeFilterToBitmap(pixels, mBmp, mFrameWidth, mFrameHeight);
		return mBmp;
	}

	@Override
	protected void setPreviewSize(int width, int height) {
		Log.i(TAG, "Preview Size is "+width + " x "+height);
		if (mBmp != null){
			mBmp.recycle();
		}
		mBmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		pixels = new int[width*height];


	}

}
