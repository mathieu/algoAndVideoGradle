package org.mathux.algoandvideo;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;

public class EdgeRenderer extends RenderView {
	
	private static final String TAG = "EdgeRender";

	Bitmap	mBmp;
	NativeTools mTools = new NativeTools();
	NativeProcessing mIp = new NativeProcessing();
	int [] pixels;


	
	public EdgeRenderer(Context context) {
		super(context);
	}

	public EdgeRenderer(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected Bitmap processFrame(byte[] data) {
		
		mIp.nv12ToRgba(data, mFrameWidth, mFrameHeight, pixels);
		mIp.edgeToBitmap(pixels, mBmp, mFrameWidth, mFrameHeight);
		return mBmp;

	}

	@Override
	protected void setPreviewSize(int width, int height) {
		Log.i(TAG, "Preview Size is " + width + " x " + height);
		if (mBmp != null){
			mBmp.recycle();
			mBmp = null;
			pixels = null;
			System.gc();
		}
		mIp.edgeInit(width, height);
		mBmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

		pixels = new int[width * height];

	}
	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		super.surfaceDestroyed(holder);
		mIp.edgeClose();
	}

}
