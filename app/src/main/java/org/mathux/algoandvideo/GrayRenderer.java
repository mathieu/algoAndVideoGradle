package org.mathux.algoandvideo;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.util.Log;

public class GrayRenderer extends RenderView {

	private static final String TAG = "GrayRender";
	Bitmap	mBmp;
	NativeTools mTools = new NativeTools();
	int [] pixels;
	
	public GrayRenderer(Context context) {
		super(context);
	}
	
	public GrayRenderer(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	
	@Override
	protected Bitmap processFrame(byte[] data) {
		/*int frameSize = getFrameHeight()*getFrameWidth();
        for (int i = 0; i < frameSize; i++) {
            int y = (0xff & ((int) data[i]));
            pixels[i] = 0xff000000 + (y << 16) + (y << 8) + y;
        } */ 
        mTools.yuv2greyBitmap(data, mFrameWidth, mFrameHeight, mBmp);
		return mBmp;
	}

	@Override
	protected void setPreviewSize(int width, int height) {
		Log.i(TAG, "Preview Size is "+width + " x "+height);
		if (mBmp != null){
			mBmp.recycle();
		}
		mBmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
	}

}
