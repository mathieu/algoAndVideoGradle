package org.mathux.algoandvideo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Toast;

public class MultiFaceRenderer extends RenderView {
	private static final String TAG = "MultiFaceRenderer";
	private static final String cascadePath =  Environment.getExternalStorageDirectory().getPath()+"/haarcascade_frontalface_alt.xml";

	Bitmap	mBmp;
	NativeTools mTools = new NativeTools();
	NativeProcessing mProcess = new NativeProcessing();
	int [] pixels;
	int [] tmpPixels;
	public OpenCV mOpencv ;
	Rect[] faces;
	Paint mPaint = new Paint();
	
	public MultiFaceRenderer(Context context) {
		super(context);
		init(context);mPaint.setStyle(Paint.Style.STROKE);
	}

	public MultiFaceRenderer(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	
	public void init(Context context){
		mOpencv = new OpenCV();
		mOpencv.initFaceDetection(cascadePath);
		mPaint.setColor(Color.RED);
		mPaint.setStrokeWidth(3);
		mPaint.setStyle(Paint.Style.STROKE);
		Toast.makeText(context, "Use in landscape mode ", Toast.LENGTH_SHORT).show();
	}

	@Override
	protected Bitmap processFrame(byte[] data) {
		mProcess.nv12ToArgb(data, mFrameWidth, mFrameHeight, tmpPixels);
		mOpencv.setSourceImage(tmpPixels, mFrameWidth, mFrameHeight);
		faces = mOpencv.findAllFaces();
		mBmp.setPixels(tmpPixels, 0, mFrameWidth, 0, 0,  mFrameWidth,  mFrameHeight);
		return mBmp;
	}
	
	@Override
	protected void drawOverlay(Canvas canvas) {
		super.drawOverlay(canvas);
		if (faces != null) {
			Rect rects[] = faces.clone();
			for (int i = 0; i < rects.length; i++) {
				Rect face = rects[i];
				canvas.drawRect(face, mPaint);
			}
		}
		
	}

	@Override
	protected void setPreviewSize(int width, int height) {
		Log.i(TAG, "Preview Size is "+width + " x "+height);
		if (mBmp != null){
			mBmp.recycle();
		}
		mBmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
		pixels = new int[width*height];
		tmpPixels = new int[width*height];

	}
		

}
