package org.mathux.algoandvideo;

import android.graphics.Bitmap;

public class NativeProcessing {
    static {
        System.loadLibrary("nativeProcessing");
    }


	public native boolean edge         (int[] src_data, int[] dest_data, int w, int h);
	public native boolean edgeToBitmap (int[] src_data, Bitmap bmp, int w, int h);
	public native boolean edgeInit     (int w, int h);
	public native boolean edgeClose    ();

	public native boolean edgeFilter(int[] src_data, int[] dest_data, int w,
			int h);
	public native boolean edgeFilterToBitmap(int[] src_data,  Bitmap bmp, int w,
			int h);

	public native void nv12ToRgba(byte[] in, int width, int height, int[] out);
	public native void nv12ToArgb(byte[] in, int width, int height, int[] out);
	public native void nv12ToBitmap(byte[] in, int width, int height, Bitmap bmp);
}
