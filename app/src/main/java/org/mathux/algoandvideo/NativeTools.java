package org.mathux.algoandvideo;

import android.graphics.Bitmap;

public class NativeTools {

    static {
        System.loadLibrary("tools");
    }

	public native void yuv2greyBitmap(byte[] in, int width, int height, Bitmap bmp);
	
}
