package org.mathux.algoandvideo;

import android.graphics.Bitmap;

public class Neon {

    static {
        System.loadLibrary("neon");
    }

	public native boolean isNeonSupported ();
	public native void    nv12Toargb      (byte[] in, int width, int height, int[] out  );
	public native void    nv12Torgba      (byte[] in, int width, int height, int[] out  );
	public native void    nv12ToBitmap    (byte[] in, int width, int height, Bitmap out );
	public native boolean edgeNeon        (int[] src_data, int[] dest_data, int w, int h);
	public native boolean edgeNeonToBitmap(int[] src_data, Bitmap bmp, int w, int h     );


}
