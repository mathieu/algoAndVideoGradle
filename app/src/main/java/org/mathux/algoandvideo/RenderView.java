package org.mathux.algoandvideo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Environment;
import android.text.format.Time;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

public abstract class RenderView extends SurfaceView implements SurfaceHolder.Callback, Runnable {
	
	private static final String TAG = "RenderView";

	private SurfaceHolder	mHolder;
	private Camera			mCamera;
	protected int			mFrameWidth;
	protected int			mFrameHeight;
	private byte[]			mFrame;
	private boolean			mThreadRun;
	private int				mImageFormat;
	private PreviewCallback mPreviewCallBack;


	private SurfaceView		mSurfaceView;
	

	
	public RenderView(Context context) {
		super(context);
		initRender();
	}

	public RenderView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initRender();
	}
	
	public void initRender(){
		mHolder = getHolder();
		mHolder.addCallback(this);
		mPreviewCallBack = new PreviewCallback() {
			public void onPreviewFrame(byte[] data, Camera camera) {
				synchronized (RenderView.this) {
					mFrame = data;
					RenderView.this.notify();
				}
			}
		};
	}
	

	public SurfaceView getSurfaceView() {
		return mSurfaceView;
	}

	public void setSurfaceView(SurfaceView mSurfaceView) {
		this.mSurfaceView = mSurfaceView;
	}

	public void startPicture() {
		mCamera.stopPreview();
		mCamera.setPreviewCallback(null);

		mCamera.takePicture(null, null, new PictureCallback() {

			@Override
			public void onPictureTaken(byte[] data, Camera camera) {
				Log.e(TAG, "onPictureTaken");
				String filename = "";
				Size size = camera.getParameters().getPictureSize();
				setPreviewSize(size.width/2, size.height/2);
				Bitmap bmp = processFrame(data);

				Time now = new Time();
				now.setToNow();
				filename = now.format2445() + ".png";

				String path = Environment.getExternalStorageDirectory()
						.getPath() +"/"+ filename;
				try {
					FileOutputStream out = new FileOutputStream(path);
					bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
					Toast.makeText(RenderView.this.getContext(),
							"Image saved under " + path +". Restart by selecting a new detection [menu]", Toast.LENGTH_LONG)
							.show();
				} catch (Exception e) {
					Toast.makeText(
							RenderView.this.getContext(),
							"Cannot save image. May be SdCard is not accessible",
							Toast.LENGTH_LONG).show();
					e.printStackTrace();
				}

			}
		});

	}

	protected abstract Bitmap processFrame(byte[] data);

	protected abstract void setPreviewSize(int width, int height);
	
	protected void drawOverlay(Canvas canvas){
		
	}

	@Override
	public void run() {
		mThreadRun = true;
		Log.i(TAG, "Starting processing thread");
		//int frameNumber = 0;
		while (mThreadRun) {
			Bitmap bmp = null;

			synchronized (this) {
				//Log.i(TAG, "Process Frame "+frameNumber++);
				try {
					this.wait();
					bmp = processFrame(mFrame);

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			if (bmp != null) {
				Canvas canvas = mHolder.lockCanvas();
				if (canvas != null) {
					canvas.drawBitmap(bmp,
							0,
							0, null);
					drawOverlay(canvas);
					mHolder.unlockCanvasAndPost(canvas);
				}
			}
			if (mCamera != null) {
				mCamera.setPreviewCallbackWithBuffer(mPreviewCallBack);
				mCamera.addCallbackBuffer(mFrame);
			}

		}

	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		synchronized (this) {
			if (mCamera != null) {
				Camera.Parameters params = mCamera.getParameters();
				List<Camera.Size> sizes = params.getSupportedPreviewSizes();
				mFrameWidth = width;
				mFrameHeight = height;

				// selecting optimal camera preview size
				{
					double minDiff = Double.MAX_VALUE;
					for (Camera.Size size : sizes) {
						if (Math.abs(size.height - height) < minDiff) {
							mFrameWidth = size.width;
							mFrameHeight = size.height;
							minDiff = Math.abs(size.height - height);
						}
					}
				}
				// select optimal preview format
				/*
				if (params.getSupportedPreviewFormats().contains(ImageFormat.RGB_565)){
					params.setPreviewFormat(ImageFormat.RGB_565);
				}*/

				params.setPreviewSize(mFrameWidth, mFrameHeight);

				mCamera.setParameters(params);
				mFrameWidth  = params.getPreviewSize().width;
				mFrameHeight = params.getPreviewSize().height;
				mImageFormat = params.getPreviewFormat();
				if (mSurfaceView != null) {
					Log.i(TAG, "Preview Ready");
					SurfaceHolder surfaceHolder = mSurfaceView.getHolder();
					surfaceHolder.addCallback(new SurfaceHolder.Callback() {

						@Override
						public void surfaceDestroyed(SurfaceHolder holder) {

						}

						@Override
						public void surfaceCreated(SurfaceHolder holder) {

						}

						@Override
						public void surfaceChanged(SurfaceHolder holder,
								int format, int width, int height) {
							try {
								mCamera.setPreviewDisplay(holder);
							} catch (IOException e) {
								Log.e(TAG, "mCamera.setPreviewDisplay fails: "
										+ e);
								e.printStackTrace();
							}

						}
					});

				}

				PixelFormat pixelinfo = new PixelFormat();
				int pixelformat = params.getPreviewFormat();
				PixelFormat.getPixelFormatInfo(pixelformat, pixelinfo);
				Size preview_size = params.getPreviewSize();

				setPreviewSize(mFrameWidth, mFrameHeight);

				mFrame = new byte[(preview_size.width * preview_size.height
						* pixelinfo.bitsPerPixel + 8) / 8];

				mCamera.addCallbackBuffer(mFrame);
				mCamera.startPreview();

			}

		}

	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Log.i(TAG, "surfaceCreated");
		mCamera = Camera.open();
		if (mCamera == null)
		{
			Toast.makeText(
					RenderView.this.getContext(),
					"Cannot open Camera. Is it already in use ? Do you have a back facing camera ?",
					Toast.LENGTH_LONG).show();
			return;
		}
		mCamera.setPreviewCallbackWithBuffer(mPreviewCallBack);
		(new Thread(this)).start();

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		mThreadRun = false;
		if (mCamera != null) {
			synchronized (this) {
				mCamera.stopPreview();
				mCamera.setPreviewCallback(null);
				mCamera.release();
				mCamera = null;
			}
		}
	}

	public int getFrameWidth() {
		return mFrameWidth;
	}

	public int getFrameHeight() {
		return mFrameHeight;
	}
	
	public int getImageFormat() {
		return mImageFormat;
	}

}
