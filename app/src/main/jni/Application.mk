#APP_PROJECT_PATH :=  $(call my-dir)
APP_MODULES      := cxcore cv cvaux cvml cvhighgui opencv tools nativeProcessing 
APP_ABI          := armeabi armeabi-v7a
APP_OPTIM        := release
APP_CFLAGS       += -Wno-error=format-security
NDK_TOOLCHAIN_VERSION := 4.9

