LOCAL_PATH := $(call my-dir)
$(info $(LOCAL_PATH))

include $(CLEAR_VARS)

LOCAL_MODULE    := nativeProcessing
LOCAL_SRC_FILES := interface.c
ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
	LOCAL_CFLAGS := -DHAVE_NEON=1
	LOCAL_SRC_FILES += colorconv.c.neon
	LOCAL_SRC_FILES += imageProcessing.c.neon
else
	LOCAL_SRC_FILES += colorconv.c
	LOCAL_SRC_FILES += imageProcessing.c
endif

LOCAL_STATIC_LIBRARIES := cpufeatures
LOCAL_C_INCLUDES := \
        $(LOCAL_PATH)/../tools/
        
LOCAL_CFLAGS += -Wall
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog -lm
LOCAL_LDLIBS += -ljnigraphics
include $(BUILD_SHARED_LIBRARY)

$(call import-module,android/cpufeatures)
