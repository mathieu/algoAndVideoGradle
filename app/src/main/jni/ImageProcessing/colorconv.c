#include "colorconv.h"
#ifdef HAVE_NEON
#include <arm_neon.h>
#endif
#include <android/log.h>

#define LOGV(...) __android_log_print(ANDROID_LOG_SILENT, LOG_TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

#define LOG_TAG "NV12"
#define likely(x) __builtin_expect((x),1)
#define unlikely(x) __builtin_expect((x),0)

#define ALPHA 0xff

/*
   1 * Y                     Y          Y           Y           255
   (u_coeff * U) / 64:	  ( 0 * U   -22 * U     113 * U        0 * U ) >> 6
   (v_coeff * V) / 64:	  (90 * V   -46 * V       0 * V        0 * V ) >> 6
 + -------------------     ------   -------     ------        ------
    result	             R         G           B           alpha
*/

#ifdef HAVE_NEON

void  nv12toargb(const unsigned char * sourcep,
		unsigned int * destp, int width, int height){

	const unsigned char *yvector_endp;
	const unsigned char *uvsourcep;
	int col, line , source_byte_count;
	const int16x8_t u_coeff = {0, -22, 113, 0, 0, -22, 113, 0};
	const int16x8_t v_coeff = {90, -46, 0,  0, 90, -46, 0,  0};

	const int16x8_t uvbias = {0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80};
	int16x8_t mp_rgba;      /* macropixel resulting RGBA RGBA pixels  */
	uint8x8_t rgba;         /* rgba values as bytes  */
	uint8_t * destinationp; /* pointer into output buffer destp  */
	const uint8x8_t yselect = {0xff, 0xff, 0xff, 0xff,
		0x00, 0x00, 0x00, 0x00};

	source_byte_count = width * height;

	yvector_endp = sourcep + source_byte_count;
	destinationp = (uint8_t *)destp;
	uvsourcep    = sourcep + source_byte_count;
	col=0;
	line = 0;

	while (sourcep < yvector_endp){
		uint8x8_t y0_vec, y1_vec;
		int16x8_t u_vec, v_vec, uv_vec;
		uint8x8_t yalpha;
		int16x8_t wider_yalpha;

		// Y0 Y0 Y0 Y0 Y0 Y0 Y0 Y0
		y0_vec = vdup_n_u8(sourcep[0]);
		y1_vec = vdup_n_u8(sourcep[1]);

		// Y0 Y0 Y0 Y0 Y1 Y1 Y1 Y1
		yalpha = vbsl_u8(yselect, y0_vec, y1_vec);


		// Y0 Y0 Y0 ALPHA Y1 Y1 Y1 ALPHA
		yalpha =  vset_lane_u8(ALPHA, yalpha, 3);
		yalpha =  vset_lane_u8(ALPHA, yalpha, 7);

		/* use vmovl_u8 to go from being unsigned 8-bit to  */
		/* unsigned 16-bit, the use vreinterpretq_s16_u16 to  */
		/* change interpretation from unsigned 16-bit to signed  */
		/* 16-bit.   */
		wider_yalpha = vreinterpretq_s16_u16(vmovl_u8(yalpha));

		u_vec = vdupq_n_s16(uvsourcep[0]);
		v_vec = vdupq_n_s16(uvsourcep[1]);

		/* subtract uvbias from u_vec and v_vec*/
		u_vec = vsubq_s16(u_vec, uvbias);
		v_vec = vsubq_s16(v_vec, uvbias);

		/* multiply by [uv]_coeff for [uv]_vec */
		u_vec = vmulq_s16(u_vec, u_coeff);
		v_vec = vmulq_s16(v_vec, v_coeff);

		uv_vec = vaddq_s16(u_vec, v_vec);
		uv_vec = vshrq_n_s16(uv_vec, 6);

		mp_rgba = vaddq_s16(wider_yalpha, uv_vec);
		rgba	= vqmovun_s16(mp_rgba);

		vst1_u8(destinationp, rgba);

		destinationp     +=8;
		sourcep   +=2;

		col +=2;
		if ( likely (col < width)){
			uvsourcep +=2;
		}else{
			col = 0;
			line ++;
			if(line & 1){
				uvsourcep -=width - 2;
			}else{
				uvsourcep +=2;
			}
		}
	}

}



void  nv12torgba(const unsigned char * sourcep,
		unsigned int * destp, int width, int height){

	const unsigned char *yvector_endp;
	const unsigned char *uvsourcep;
	int col, line , source_byte_count;
	/*const int16x8_t u_coeff = {0, -22, 113, 0, 0, -22, 113, 0};
	const int16x8_t v_coeff = {90, -46, 0,  0, 90, -46, 0,  0};*/
	const int16x8_t u_coeff = {113, -22, 0, 0, 113, -22, 0, 0};
	const int16x8_t v_coeff = {0, -46, 90,  0, 0, -46, 90,  0};
	const int16x8_t uvbias = {0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80};
	int16x8_t mp_rgba;      /* macropixel resulting RGBA RGBA pixels  */
	uint8x8_t rgba;         /* rgba values as bytes  */
	uint8_t * destinationp; /* pointer into output buffer destp  */
	const uint8x8_t yselect = {0xff, 0xff, 0xff, 0xff,
		0x00, 0x00, 0x00, 0x00};

	source_byte_count = width * height;

	yvector_endp = sourcep + source_byte_count;
	destinationp = (uint8_t *)destp;
	uvsourcep    = sourcep + source_byte_count;
	col=0;
	line = 0;

	while (sourcep < yvector_endp){
		uint8x8_t y0_vec, y1_vec;
		int16x8_t u_vec, v_vec, uv_vec;
		uint8x8_t yalpha;
		int16x8_t wider_yalpha;

		// Y0 Y0 Y0 Y0 Y0 Y0 Y0 Y0
		y0_vec = vdup_n_u8(sourcep[0]);
		y1_vec = vdup_n_u8(sourcep[1]);

		// Y0 Y0 Y0 Y0 Y1 Y1 Y1 Y1
		yalpha = vbsl_u8(yselect, y0_vec, y1_vec);


		// Y0 Y0 Y0 ALPHA Y1 Y1 Y1 ALPHA
		yalpha =  vset_lane_u8(ALPHA, yalpha, 3);
		yalpha =  vset_lane_u8(ALPHA, yalpha, 7);

		/* use vmovl_u8 to go from being unsigned 8-bit to  */
		/* unsigned 16-bit, the use vreinterpretq_s16_u16 to  */
		/* change interpretation from unsigned 16-bit to signed  */
		/* 16-bit.   */
		wider_yalpha = vreinterpretq_s16_u16(vmovl_u8(yalpha));

		u_vec = vdupq_n_s16(uvsourcep[0]);
		v_vec = vdupq_n_s16(uvsourcep[1]);

		/* subtract uvbias from u_vec and v_vec*/
		u_vec = vsubq_s16(u_vec, uvbias);
		v_vec = vsubq_s16(v_vec, uvbias);

		/* multiply by [uv]_coeff for [uv]_vec */
		u_vec = vmulq_s16(u_vec, u_coeff);
		v_vec = vmulq_s16(v_vec, v_coeff);

		uv_vec = vaddq_s16(u_vec, v_vec);
		uv_vec = vshrq_n_s16(uv_vec, 6);

		mp_rgba = vaddq_s16(wider_yalpha, uv_vec);
		rgba	= vqmovun_s16(mp_rgba);

		vst1_u8(destinationp, rgba);

		destinationp     +=8;
		sourcep   +=2;

		col +=2;
		if ( likely (col < width)){
			uvsourcep +=2;
		}else{
			col = 0;
			line ++;
			if(line & 1){
				uvsourcep -=width - 2;
			}else{
				uvsourcep +=2;
			}
		}
	}

}
#else

void nv12toargb(const unsigned char * yuv420sp, unsigned int * rgb, int width, int height){
int frameSize = width * height;
int j, i, yp;

	for (j = 0, yp = 0; j < height; j++) {
		int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
		for (i = 0; i < width; i++, yp++) {
			int y = (0xff & ((int) yuv420sp[yp])) - 16;
			if (y < 0) y = 0;
			if ((i & 1) == 0) {
				v = (0xff & yuv420sp[uvp++]) - 128;
				u = (0xff & yuv420sp[uvp++]) - 128;
			}

			int y1192 = 1192 * y;
			int r = (y1192 + 1634 * v);
			int g = (y1192 - 833 * v - 400 * u);
			int b = (y1192 + 2066 * u);

			if (r < 0) r = 0; else if (r > 262143) r = 262143;
			if (g < 0) g = 0; else if (g > 262143) g = 262143;
			if (b < 0) b = 0; else if (b > 262143) b = 262143;

			rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
		}
	}
}

void nv12torgba(const unsigned char * yuv420sp, unsigned int * rgb, int width, int height){
	int frameSize = width * height;
	int j, i, yp;

   	for (j = 0, yp = 0; j < height; j++) {
    		int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
    		for (i = 0; i < width; i++, yp++) {
    			int y = (0xff & ((int) yuv420sp[yp])) - 16;
    			if (y < 0) y = 0;
    			if ((i & 1) == 0) {
    				v = (0xff & yuv420sp[uvp++]) - 128;
    				u = (0xff & yuv420sp[uvp++]) - 128;
    			}

    			int y1192 = 1192 * y;
    			int r = (y1192 + 1634 * v);
    			int g = (y1192 - 833 * v - 400 * u);
    			int b = (y1192 + 2066 * u);

    			if (r < 0) r = 0; else if (r > 262143) r = 262143;
    			if (g < 0) g = 0; else if (g > 262143) g = 262143;
    			if (b < 0) b = 0; else if (b > 262143) b = 262143;

    			//rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
    			rgb[yp] = 0xff000000 | ((b << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((r >> 10) & 0xff);
    		}
    	}
}


#endif//HAVE_NEON



