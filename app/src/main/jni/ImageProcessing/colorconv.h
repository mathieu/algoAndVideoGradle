#ifndef COLORCONV_H
#define COLORCONV_H

void nv12torgba(const unsigned char * sourcep,    unsigned int * destp, int width, int height);
void nv12toargb(const unsigned char * sourcep,    unsigned int * destp, int width, int height);

#endif /* COLORCONV */
