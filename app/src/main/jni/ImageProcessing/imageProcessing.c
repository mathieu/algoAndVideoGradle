#include <string.h>
#include <jni.h>
#include <imageProcessing.h>
#include <math.h>
#include <tools.h>
#ifdef HAVE_NEON
#include <arm_neon.h>
#endif
#define LOG_TAG "ImageProcessing"


unsigned int * map;
#ifdef HAVE_NEON

void edgeFilter(int* src, int * dst, int width, int height) {
	//LOGE("NEON VERSION");
	int x, y, row, col;
    int index = 0;

    int16x4_t vEdgeMatrix_vect[] = {{-1, -1, -1, -1}, {0, 0, 0, 0}    , {1, 1, 1, 1}    , {-2, -2, -2, -2}, {0, 0, 0, 0}, {2, 2, 2 ,2}, {-1, -1, -1, -1},  {0, 0 ,0 ,0}, {1, 1, 1, 1}};
    int16x4_t hEdgeMatrix_vect[] = {{-1, -1, -1, -1}, {-2, -2, -2, -2}, {-1, -1, -1, -1}, {0, 0, 0, 0}    , {0, 0, 0, 0}, {0, 0, 0, 0}, {1, 1, 1, 1}    ,  {2, 2, 2 ,2}, {1, 1, 1, 1}};
    uint32x4_t cst_vect = {158,158,158,158};
	int r, g, b;
	uint32x4_t argbv2_vect;
	uint32x4_t argbh2_vect;
	uint32x4_t res ;
	uint32_t argb[4];


	for (y = 0; y < height; y++) {
		for (x = 0; x < width; x++) {
			int16x4_t argbh_vect = {0,0,0,0};
			int16x4_t argbv_vect = {0,0,0,0};

			for (row = -1; row <= 1; row++) {
				int iy = y+row;
				int ioffset;
				if (0 <= iy && iy < height)
					ioffset = iy*width;
				else
					ioffset = y*width;
				int moffset = 3*(row+1)+1;
				for (col = -1; col <= 1; col++) {
					int ix = x+col;
					if (!(0 <= ix && ix < width))
						ix = x;
					int rgb = src[ioffset+ix];

					int16x4_t h_vect = hEdgeMatrix_vect[moffset+col];
					int16x4_t v_vect = vEdgeMatrix_vect[moffset+col];

					int16x4_t argb_vect = {0x0, (rgb & 0xff0000) >> 16, (rgb & 0x00ff00) >> 8, rgb & 0x0000ff};
					if (row != 0){ //otherwise h_vect is 0
						argbh_vect = vmla_s16(argbh_vect, argb_vect, h_vect);
					}
					if (col != 0){ // otherwise v_vect is 0
						argbv_vect = vmla_s16(argbv_vect, argb_vect, v_vect);
					}

				}
			}

			argbh2_vect = vreinterpretq_u32_s32(vmull_s16(argbh_vect, argbh_vect));
			argbv2_vect = vreinterpretq_u32_s32(vmull_s16(argbv_vect, argbv_vect));

			res = vaddq_u32(argbh2_vect, argbv2_vect);

			// Could be replace by res = vshrq_n_u32(res, 3);
			// Gain < 10% but image
			res = vmulq_u32(res, cst_vect);
			res = vshrq_n_u32(res, 9);


			vst1q_u32(argb, res);

			r = sqrt(argb[1]);
			g = sqrt(argb[2]);
			b = sqrt(argb[3]);

            if(r>255) r = 255;
            if(g>255) g = 255;
            if(b>255) b = 255;
			dst[index++] =  0xff000000 | (r << 16) | (g << 8) | b;
		}
    }
}

//NEON not as quick as expected for this algo
void edge(int* src, int * dest, int width, int height) {
	int x, y;
	int r, g, b;
	unsigned int p, q;
	unsigned int v0, v1, v2, v3;

    int map_width = width / 4;
    int map_height = height / 4;
    int width_margin = width - map_width * 4;
#ifdef EDGE_USE_NEON
    int32x4_t max = {255,255,255,255};
    unsigned int q2;
#endif
    //map = (unsigned int *)sharedbuffer_alloc(map_width*map_height*PIXEL_SIZE*2);
    if(map == NULL){
    	LOGE("Edge function not initialize !!");
        return ;
    }

    //memset(map, 0, width * height * PIXEL_SIZE * 2);
	src += width*4+4;
	dest += width*4+4;
	for(y=1; y<map_height-1; y++) {
		for(x=1; x<map_width-1; x++) {
			p = *src;
			q = *(src - 4);
#ifdef EDGE_USE_NEON
			q2 = *(src - width*4);

/* difference between the current pixel and right neighbor. */
			int32x4_t p_vect = {0xff, (p & 0xff0000) >> 16, (p & 0x00ff00) >> 8, p & 0x0000ff };
			int32x4_t q_vect = {0xff, (q & 0xff0000) >> 16, (q & 0x00ff00) >> 8, q & 0x0000ff };
			int32x4_t q2_vect = {0xff, (q2 & 0xff0000) >> 16, (q2 & 0x00ff00) >> 8, q2 & 0x0000ff };
			int32x4_t c;
			int res[4];

			c = vsubq_s32(p_vect,q_vect);
			c = vmulq_s32(c,c);
			c = vshrq_n_s32(c,4);
			c = vminq_s32(c, max);

			vst1q_s32(res, c);
			v2 = 0xff000000 | (res[1]>>1) << 17 | (res[2]>>1) << 9 | res[3];

			c = vsubq_s32(p_vect,q2_vect);
			c = vmulq_s32(c,c);
			c = vshrq_n_s32(c,4);
			c = vminq_s32(c, max);

			vst1q_s32(res, c);
			v3 = 0xff000000 | (res[1]>>1) << 17 | (res[2]>>1) << 9 | res[3];
#else
			r = ((int)(p & 0xff0000) - (int)(q & 0xff0000))>>16;
			g = ((int)(p & 0x00ff00) - (int)(q & 0x00ff00))>>8;
			b = ((int)(p & 0x0000ff) - (int)(q & 0x0000ff));
			r *= r; /* Multiply itself and divide it with 16, instead of */
			g *= g; /* using abs(). */
			b *= b;
			r = r>>5; /* To lack the lower bit for saturated addition,  */
			g = g>>5; /* devide the value with 32, instead of 16. It is */
			b = b>>4; /* same as `v2 &= 0xfefeff' */
			if(r>127) r = 127;
			if(g>127) g = 127;
			if(b>255) b = 255;
			v2 = 0xff000000|(r<<17)|(g<<9)|b;

/* difference between the current pixel and upper neighbor. */
			q = *(src - width*4);
			r = ((int)(p & 0xff0000) - (int)(q & 0xff0000))>>16;
			g = ((int)(p & 0x00ff00) - (int)(q & 0x00ff00))>>8;
			b = ((int)(p & 0x0000ff) - (int)(q & 0x0000ff));
			r *= r;
			g *= g;
			b *= b;
			r = r>>5;
			g = g>>5;
			b = b>>4;
			if(r>127) r = 127;
			if(g>127) g = 127;
			if(b>255) b = 255;
			v3 = 0xff000000|(r<<17)|(g<<9)|b;
#endif //USE_NEON

			v0 = map[(y-1)*map_width*2+x*2];
			v1 = map[y*map_width*2+(x-1)*2+1];
			map[y*map_width*2+x*2] = v2;
			map[y*map_width*2+x*2+1] = v3;
			r = v0 + v1;
			g = r & 0x01010100;
			dest[0] = r | (g - (g>>8));
			r = v0 + v3;
			g = r & 0x01010100;
			dest[1] = r | (g - (g>>8));
			dest[2] = v3;
			dest[3] = v3;
			r = v2 + v1;
			g = r & 0x01010100;
			dest[width] = r | (g - (g>>8));
			r = v2 + v3;
			g = r & 0x01010100;
			dest[width+1] = r | (g - (g>>8));
			dest[width+2] = v3;
			dest[width+3] = v3;
			dest[width*2] = v2;
			dest[width*2+1] = v2;
			dest[width*3] = v2;
			dest[width*3+1] = v2;

			src += 4;
			dest += 4;
		}
		src += width*3+8+width_margin;
		dest += width*3+8+width_margin;
	}
}
#else

void edgeFilter(int* src, int * dst, int width, int height) {
	int x, y, row, col;
    int index = 0;

    int vEdgeMatrix[] = {-1,  0,  1, -2,  0,  2, -1,  0,  1};
    int hEdgeMatrix[] = {-1, -2, -1,  0,  0,  0,  1,  2,  1};



	for (y = 0; y < height; y++) {
		for (x = 0; x < width; x++) {
			int r = 0, g = 0, b = 0;
			int rh = 0, gh = 0, bh = 0;
			int rv = 0, gv = 0, bv = 0;

			for (row = -1; row <= 1; row++) {
				int iy = y+row;
				int ioffset;
				if (0 <= iy && iy < height)
					ioffset = iy*width;
				else
					ioffset = y*width;
				int moffset = 3*(row+1)+1;
				for (col = -1; col <= 1; col++) {
					int ix = x+col;
					if (!(0 <= ix && ix < width))
						ix = x;
					int rgb = src[ioffset+ix];
					int h = hEdgeMatrix[moffset+col];
					int v = vEdgeMatrix[moffset+col];

					r = (rgb & 0xff0000) >> 16;
					g = (rgb & 0x00ff00) >> 8;
					b = rgb & 0x0000ff;
					rh += (int)(h * r);
					gh += (int)(h * g);
					bh += (int)(h * b);
					rv += (int)(v * r);
					gv += (int)(v * g);
					bv += (int)(v * b);
				}
			}
			r = (int)(sqrt(((rh*rh + rv*rv)*158)>>9));
			g = (int)(sqrt(((gh*gh + gv*gv)*158)>>9));
			b = (int)(sqrt(((bh*bh + bv*bv)*158)>>9));
			if(r<0) r = 0;
			if(g<0) g = 0;
			if(b<0) b = 0;
            if(r>255) r = 255;
            if(g>255) g = 255;
            if(b>255) b = 255;
			dst[index++] =  0xff000000 | (r << 16) | (g << 8) | b;
		}
    }
}

void edge(int* src, int * dest, int width, int height) {
	int x, y;
	int r, g, b;
	unsigned int p, q;
	unsigned int v0, v1, v2, v3;


    int map_width = width / 4;
    int map_height = height / 4;
    int width_margin = width - map_width * 4;
    //map = (unsigned int *)sharedbuffer_alloc(map_width*map_height*PIXEL_SIZE*2);
    if(map == NULL){
    	LOGE("Edge function not initialize !!");
        return ;
    }

    //memset(map, 0, width * height * PIXEL_SIZE * 2);
	src += width*4+4;
	dest += width*4+4;
	for(y=1; y<map_height-1; y++) {
		for(x=1; x<map_width-1; x++) {
			p = *src;
			q = *(src - 4);

/* difference between the current pixel and right neighbor. */
			r = ((int)(p & 0xff0000) - (int)(q & 0xff0000))>>16;
			g = ((int)(p & 0x00ff00) - (int)(q & 0x00ff00))>>8;
			b = ((int)(p & 0x0000ff) - (int)(q & 0x0000ff));
			r *= r; /* Multiply itself and divide it with 16, instead of */
			g *= g; /* using abs(). */
			b *= b;
			r = r>>5; /* To lack the lower bit for saturated addition,  */
			g = g>>5; /* devide the value with 32, instead of 16. It is */
			b = b>>4; /* same as `v2 &= 0xfefeff' */
			if(r>127) r = 127;
			if(g>127) g = 127;
			if(b>255) b = 255;
			v2 = 0xff000000|(r<<17)|(g<<9)|b;

/* difference between the current pixel and upper neighbor. */
			q = *(src - width*4);
			r = ((int)(p & 0xff0000) - (int)(q & 0xff0000))>>16;
			g = ((int)(p & 0x00ff00) - (int)(q & 0x00ff00))>>8;
			b = ((int)(p & 0x0000ff) - (int)(q & 0x0000ff));
			r *= r;
			g *= g;
			b *= b;
			r = r>>5;
			g = g>>5;
			b = b>>4;
			if(r>127) r = 127;
			if(g>127) g = 127;
			if(b>255) b = 255;
			v3 = 0xff000000|(r<<17)|(g<<9)|b;

			v0 = map[(y-1)*map_width*2+x*2];
			v1 = map[y*map_width*2+(x-1)*2+1];
			map[y*map_width*2+x*2] = v2;
			map[y*map_width*2+x*2+1] = v3;
			r = v0 + v1;
			g = r & 0x01010100;
			dest[0] = r | (g - (g>>8));
			r = v0 + v3;
			g = r & 0x01010100;
			dest[1] = r | (g - (g>>8));
			dest[2] = v3;
			dest[3] = v3;
			r = v2 + v1;
			g = r & 0x01010100;
			dest[width] = r | (g - (g>>8));
			r = v2 + v3;
			g = r & 0x01010100;
			dest[width+1] = r | (g - (g>>8));
			dest[width+2] = v3;
			dest[width+3] = v3;
			dest[width*2] = v2;
			dest[width*2+1] = v2;
			dest[width*3] = v2;
			dest[width*3+1] = v2;

			src += 4;
			dest += 4;
		}
		src += width*3+8+width_margin;
		dest += width*3+8+width_margin;
	}
}



#endif // HAVE_NEON
