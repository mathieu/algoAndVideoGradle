#include <string.h>
#include <jni.h>
#include <imageProcessing.h>
#include <math.h>
#include <tools.h>
#include <colorconv.h>
#include <imageProcessing.h>
#include <stdint.h>
#ifdef HAVE_NEON
#include <cpu-features.h>
#endif
#include <android/bitmap.h>

#define LOG_TAG "interface"
extern unsigned int * map;

JNIEXPORT
jboolean
JNICALL
Java_org_mathux_algoandvideo_NativeProcessing_edgeInit(JNIEnv* env,
		jobject thiz, jint width, jint height){
	size_t PIXEL_SIZE = sizeof(int);
	map = (unsigned int *)malloc(width*height*PIXEL_SIZE*2);
	memset(map, 0, width * height * PIXEL_SIZE * 2);
	return JNI_FALSE;
}

JNIEXPORT
jboolean
JNICALL
Java_org_mathux_algoandvideo_NativeProcessing_edgeClose(JNIEnv* env,
		jobject thiz){
    free(map);
    return JNI_FALSE;
}

JNIEXPORT
jboolean
JNICALL
Java_org_mathux_algoandvideo_NativeProcessing_edge(JNIEnv* env,
										jobject thiz,
										jintArray photo_data,
                                        jintArray photo_dest,
										jint width,
										jint height) {

	DECLARE_JINT_JINT_ARRAY_AND_CONVERT_JNI(env, src, dest, photo_data, photo_dest)
    edge(src, dest, width, height);
	RELEASE_JINT_JINT_ARRAY(env, src, dest, photo_data, photo_dest)

	return JNI_FALSE;

}

JNIEXPORT
jboolean
JNICALL
Java_org_mathux_algoandvideo_NativeProcessing_edgeToBitmap(JNIEnv* env,
										jobject thiz,
										jintArray pinArray,
                                        jobject bitmap,
										jint width,
										jint height) {

	DECLARE_JINT_ARRAY_BITMAP_AND_CONVERT_JNI(env, src, dest, pinArray, bitmap)
    edge(src, dest, width, height);
	RELEASE_JINT_ARRAY_BITMAP(env, src, bitmap, pinArray)

	return JNI_FALSE;

}


JNIEXPORT
jboolean
JNICALL
Java_org_mathux_algoandvideo_NativeProcessing_edgeFilter(JNIEnv* env,
										jobject thiz,
										jintArray photo_data,
                                        jintArray photo_dest,
										jint width,
										jint height) {

	DECLARE_JINT_JINT_ARRAY_AND_CONVERT_JNI(env, src, dest, photo_data, photo_dest)
    edgeFilter(src, dest, width, height);
	RELEASE_JINT_JINT_ARRAY(env, src, dest, photo_data, photo_dest)

    return JNI_FALSE;

}

JNIEXPORT
jboolean
JNICALL
Java_org_mathux_algoandvideo_NativeProcessing_edgeFilterToBitmap(JNIEnv* env,
										jobject thiz,
										jintArray pinArray,
                                        jobject bitmap,
										jint width,
										jint height) {

	DECLARE_JINT_ARRAY_BITMAP_AND_CONVERT_JNI(env, src, dest, pinArray, bitmap)
    edgeFilter(src, dest, width, height);
	RELEASE_JINT_ARRAY_BITMAP(env, src, bitmap, pinArray)

    return JNI_FALSE;

}
JNIEXPORT
jboolean
JNICALL
Java_org_mathux_algoandvideo_NativeProcessing_isNeonSupported(JNIEnv* env,
		jobject thiz){
#ifdef HAVE_NEON
	{
		uint64_t features;
		if (android_getCpuFamily() != ANDROID_CPU_FAMILY_ARM) {
			return JNI_FALSE;
		}

		features = android_getCpuFeatures();
		if ((features & ANDROID_CPU_ARM_FEATURE_ARMv7) == 0) {
			return JNI_FALSE;
		}

		/* HAVE_NEON is defined in Android.mk ! */
		if ((features & ANDROID_CPU_ARM_FEATURE_NEON) == 0) {
			return JNI_FALSE;
		}
		return JNI_TRUE;
	}
#else
	return JNI_FALSE;
#endif

}


JNIEXPORT void JNICALL Java_org_mathux_algoandvideo_NativeProcessing_nv12ToBitmap
(JNIEnv* env, jobject object, jbyteArray pinArray, jint width, jint height, jobject bitmap) {

	DECLARE_JBYTE_ARRAY_BITMAP_AND_CONVERT_JNI(env, inArray, outArray, pinArray, bitmap)
	nv12torgba((unsigned char *)inArray, (unsigned int *)outArray, width, height);
	RELEASE_JBYTE_ARRAY_BITMAP(env, inArray, bitmap, pinArray)

}

JNIEXPORT void JNICALL Java_org_mathux_algoandvideo_NativeProcessing_nv12ToArgb
  (JNIEnv* env, jobject object, jbyteArray pinArray, jint width, jint height, jintArray poutArray) {

	DECLARE_JBYTE_JINT_ARRAY_AND_CONVERT_JNI(env, inArray, outArray, pinArray, poutArray)
	nv12toargb((unsigned char *)inArray, (unsigned int *)outArray, width, height);
	RELEASE_JBYTE_JINT_ARRAY(env, inArray, outArray, pinArray, poutArray)
}

JNIEXPORT void JNICALL Java_org_mathux_algoandvideo_NativeProcessing_nv12ToRgba
  (JNIEnv* env, jobject object, jbyteArray pinArray, jint width, jint height, jintArray poutArray) {

	DECLARE_JBYTE_JINT_ARRAY_AND_CONVERT_JNI(env, inArray, outArray, pinArray, poutArray)
	nv12torgba((unsigned char *)inArray, (unsigned int *)outArray, width, height);
	RELEASE_JBYTE_JINT_ARRAY(env, inArray, outArray, pinArray, poutArray)
}
