#color conversion lib
LOCAL_PATH := $(call my-dir)
$(info $(LOCAL_PATH))

include $(CLEAR_VARS)

LOCAL_MODULE    := tools
LOCAL_SRC_FILES := yuv420sp2rgb.c tools.c
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog
LOCAL_LDLIBS += -ljnigraphics
LOCAL_CFLAGS += -Wall
include $(BUILD_SHARED_LIBRARY)
