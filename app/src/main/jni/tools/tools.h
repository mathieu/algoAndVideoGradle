#ifndef TOOLS_H
#define TOOLS_H
#include <jni.h>
#include <android/log.h>
#include <android/bitmap.h>


#ifndef max
#define max(a,b) ((a) > (b) ? (a) : (b) )
#endif
#ifndef min
#define min(a,b) ((a) < (b) ? (a) : (b) )
#endif

#define LOGV(...) __android_log_print(ANDROID_LOG_SILENT, LOG_TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

#define DECLARE_JBYTE_JINT_ARRAY_AND_CONVERT_JNI(env, in, out, jin, jout)\
	jbyte *(in) = (*(env))->GetByteArrayElements(env, (jin), JNI_FALSE);\
	jint  *(out) = (*(env))->GetIntArrayElements (env, (jout), JNI_FALSE);

#define DECLARE_JINT_JINT_ARRAY_AND_CONVERT_JNI(env, in, out, jin, jout)\
	jint *(in)  = (*(env))->GetIntArrayElements(env, (jin), JNI_FALSE);\
	jint *(out) = (*(env))->GetIntArrayElements(env, (jout), JNI_FALSE);

#define DECLARE_JBYTE_JBYTE_ARRAY_AND_CONVERT_JNI(env, in, out, jin, jout)\
	jbyte *(in) = (*(env))->GetByteArrayElements(env, (jin), JNI_FALSE);\
	jbyte *(out) = (*(env))->GetByteArrayElements(env, (jout), JNI_FALSE);

#define DECLARE_JINT_ARRAY_BITMAP_AND_CONVERT_JNI(env, in, jout, jin, bitmap)\
	jint *(in) = (*env)->GetIntArrayElements(env, (jin), JNI_FALSE);\
	jint *jout;\
	AndroidBitmap_lockPixels(env, (bitmap), (void **)(&jout));

#define DECLARE_JBYTE_ARRAY_BITMAP_AND_CONVERT_JNI(env, in, jout, jin, bitmap)\
	jbyte *(in) = (*env)->GetByteArrayElements(env, (jin), JNI_FALSE);\
	jint *jout;\
	AndroidBitmap_lockPixels(env, (bitmap), (void **)(&jout));

#define RELEASE_JBYTE_JINT_ARRAY(env, in, out, jin, jout)\
	(*env)->ReleaseByteArrayElements(env, jin, in, 0);\
	(*env)->ReleaseIntArrayElements (env, jout, out, 0);

#define RELEASE_JBYTE_JBYTE_ARRAY(env, in, out, jin,jout)\
	(*env)->ReleaseByteArrayElements(env, jin, in, 0);\
	(*env)->ReleaseByteArrayElements(env, jout, out, 0);

#define RELEASE_JINT_JINT_ARRAY(env, in, out, jin, jout)\
	(*env)->ReleaseIntArrayElements(env, jin, in, 0);\
	(*env)->ReleaseIntArrayElements (env, jout, out, 0);

#define RELEASE_JINT_ARRAY_BITMAP(env, in, bitmap, jin)\
	AndroidBitmap_unlockPixels((env), (bitmap));\
	(*env)->ReleaseIntArrayElements(env, jin, in, 0);

#define RELEASE_JBYTE_ARRAY_BITMAP(env, in, bitmap, jin)\
	AndroidBitmap_unlockPixels((env), (bitmap));\
	(*env)->ReleaseByteArrayElements(env, jin, in, 0);

static inline int checkBitmapType(JNIEnv* env,jobject bitmap){
	AndroidBitmapInfo  info;

	AndroidBitmap_getInfo(env, bitmap, &info);

	if(info.format != ANDROID_BITMAP_FORMAT_RGBA_8888) {
		__android_log_print(ANDROID_LOG_ERROR, "TOOLS", "Bitmap format is not RGBA_8888!");
	  return 0;
	}
	return 1;
}

static inline void LockBitmapPixels
(JNIEnv* env,jobject bitmap, jint*  pixels) {
	AndroidBitmap_lockPixels(env, bitmap, (void **)(&pixels));

}

static inline void UnlockBitmapPixels
(JNIEnv* env, jobject object,jobject bitmap) {
	AndroidBitmap_unlockPixels(env, bitmap);
}

#endif
