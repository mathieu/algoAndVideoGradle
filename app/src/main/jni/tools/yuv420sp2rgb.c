#include <string.h>
#include <jni.h>
#include <yuv420sp2rgb.h>
#include <tools.h>

#define LOG_TAG "NativeTools"

static void color_convert_common(
    unsigned char *pY, unsigned char *pUV,
    int width, int height, int texture_size,
    unsigned char *buffer,
    int size, /* buffer size in bytes */
    int gray,
    int rotate);

static void yuv2rgb(int * rgb, unsigned char * data, int width, int height);
static void yuv2grey(int * pixels, unsigned char * data, int width, int height);

/*
 * convert a yuv420 array to a rgb array
 */
JNIEXPORT void JNICALL Java_org_mathux_algoandvideo_NativeTools_yuv420sp2rgb
  (JNIEnv* env, jobject object, jbyteArray pinArray, jint width, jint height, jint textureSize, jbyteArray poutArray) {
	DECLARE_JBYTE_JBYTE_ARRAY_AND_CONVERT_JNI(env, inArray, outArray, pinArray, poutArray)
	
    color_convert_common((unsigned char *)inArray, (unsigned char *)(inArray + width * height),
                 width, height, textureSize,
                 (unsigned char *)outArray, width * height * 3,
                 0, 0);
	RELEASE_JBYTE_JBYTE_ARRAY(env, inArray, outArray, pinArray, poutArray)
}

JNIEXPORT void JNICALL Java_org_mathux_algoandvideo_NativeTools_yuv2rgb
  (JNIEnv* env, jobject object, jbyteArray pinArray, jint width, jint height, jintArray poutArray) {

	DECLARE_JBYTE_JINT_ARRAY_AND_CONVERT_JNI(env, inArray, outArray, pinArray, poutArray)
	yuv2rgb(outArray, (unsigned char *)inArray, width, height);
	RELEASE_JBYTE_JINT_ARRAY(env, inArray, outArray, pinArray, poutArray)

}

JNIEXPORT void JNICALL Java_org_mathux_algoandvideo_NativeTools_yuv2greyBitmap
  (JNIEnv* env, jobject object, jbyteArray pinArray, jint width, jint height, jobject bitmap) {

	DECLARE_JINT_ARRAY_BITMAP_AND_CONVERT_JNI(env, inArray, outArray, pinArray, bitmap)
	yuv2grey(outArray, (unsigned char *)inArray, width, height);
	RELEASE_JINT_ARRAY_BITMAP(env, inArray, bitmap, pinArray)

}


static void yuv2rgb(int * rgb, unsigned char * data, int width, int height){
	int i, j, y, u, v, r, g, b ;
	int frameSize = width * height ;
    for (i = 0; i < height; i++)
        for (j = 0; j < width; j++) {
            y = (0xff & ( data[i * width + j]));
            u = (0xff & ( data[frameSize + (i >> 1) * width + (j & ~1) + 0]));
            v = (0xff & ( data[frameSize + (i >> 1) * width + (j & ~1) + 1]));
            y = y < 16 ? 16 : y;

            r = (1191 * (y - 16) + 1634 * (v - 128))>>10;
            g = (1191 * (y - 16) - 832  * (v - 128) - 400 * (u - 128))>>10;
            b = (1191 * (y - 16) + 2066 * (u - 128))>>10;

            r = r < 0 ? 0 : (r > 255 ? 255 : r);
            g = g < 0 ? 0 : (g > 255 ? 255 : g);
            b = b < 0 ? 0 : (b > 255 ? 255 : b);

            rgb[i * width + j] = 0xff000000 + (b << 16) + (g << 8) + r;
        }

}

static void yuv2grey(int * pixels, unsigned char * data, int width, int height){
	int frameSize = width*height;
	int i,y;
    for ( i = 0; i < frameSize; i++) {
        y = (0xff & ((int) data[i]));
        pixels[i] = 0xff000000 + (y << 16) + (y << 8) + y;
    }
}

const int bytes_per_pixel = 2;

static void color_convert_common(
    unsigned char *pY, unsigned char *pUV,
    int width, int height, int texture_size,
    unsigned char *buffer,
    int size, /* buffer size in bytes */
    int gray,
    int rotate) 
{
	int i, j;
	int nR, nG, nB;
	int nY, nU, nV;
	unsigned char *out = buffer;
	int offset = 0;
	// YUV 4:2:0
	for (i = 0; i < height; i++) {
	    for (j = 0; j < width; j++) {
		nY = *(pY + i * width + j);
		nV = *(pUV + (i/2) * width + bytes_per_pixel * (j/2));
		nU = *(pUV + (i/2) * width + bytes_per_pixel * (j/2) + 1);
	    
		// Yuv Convert
		nY -= 16;
		nU -= 128;
		nV -= 128;
	    
		if (nY < 0)
		    nY = 0;
	    
		// nR = (int)(1.164 * nY + 2.018 * nU);
		// nG = (int)(1.164 * nY - 0.813 * nV - 0.391 * nU);
		// nB = (int)(1.164 * nY + 1.596 * nV);
	    
		nB = (int)(1192 * nY + 2066 * nU);
		nG = (int)(1192 * nY - 833 * nV - 400 * nU);
		nR = (int)(1192 * nY + 1634 * nV);
	    
		nR = min(262143, max(0, nR));
		nG = min(262143, max(0, nG));
		nB = min(262143, max(0, nB));
	    
		nR >>= 10; nR &= 0xff;
		nG >>= 10; nG &= 0xff;
		nB >>= 10; nB &= 0xff;
		out[offset++] = (unsigned char)nR;
		out[offset++] = (unsigned char)nG;
		out[offset++] = (unsigned char)nB;
	    }
	    //offset = i * width * 3;        //non power of two
	    //offset = i * texture_size + j;//power of two
	    //offset *= 3; //3 byte per pixel
	    //out = buffer + offset;
	}
}
